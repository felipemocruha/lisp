{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Lib
    ( someFunc
    ) where

import qualified Data.Text as T
import Text.Read (readMaybe)

someFunc :: IO ()
someFunc = putStrLn "someFunc"

openP = T.pack "("
closeP = T.pack ")"
tkopenP = T.pack "("
tkcloseP = T.pack ")"

replaceParens :: T.Text -> T.Text
replaceParens s = T.replace openP tkopenP (T.replace closeP tkcloseP s)
  
tokenize :: T.Text -> [T.Text]
tokenize s = do
  let empty = (/= (T.pack ""))
  filter empty $ T.split (==' ') $ replaceParens s

toInt :: T.Text -> Maybe Int
toInt tk = readMaybe $ T.unpack tk

toFloat :: T.Text -> Maybe Double
toFloat tk = readMaybe $ T.unpack tk

data Symbol = Text
data Number = Real
data List = Vector

class Atom a where
  atom :: a -> Either Number Symbol

instance Atom Number where
  atom x = Left (read (T.unpack x) :: Number)

instance Atom Symbol where
  atom x = Right x

readFromTokens :: (Expression e) => [T.Text] -> Either String e
readFromTokens ts = do
  case length ts of
    0 -> Left "syntax error: unexpected EOF"
    _ -> case head ts of
      openP -> Right T.takeWhile (==')') $ readFromTokens $ tail ts
      closeP -> Left "syntax error: unexpected ')'"
      _ -> atom ts

parse :: (Expression e) => T.Text -> e
parse s = readFromTokens $ tokenize s

-- class Expression e where
--   eval :: e -> e

-- instance Expression Real where
--   eval x = x
    

-- env "+" = (+)
-- env "-" = (-)
-- env "*" = (*)
-- env "/" = (/)
-- env ">" = (>)
-- env "<" = (<)
-- env ">=" = (>=)
-- env "<=" = (<=)
-- env "=" = (==)
-- env "abs" = abs
-- env "first" = head
-- env "rest" = tail
-- env "cons" = (\x xs -> x:xs)
-- env "len" = length
-- env "map" = map
-- env "filter" = filter
-- env "reduce" = foldl
-- env "max" = max
-- env "min" = min
-- env "not" = not
